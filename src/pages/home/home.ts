import { Component } from '@angular/core';
import {LoadingController, NavController, AlertController} from 'ionic-angular';
import {Car} from '../../models/car';
import {HttpErrorResponse} from '@angular/common/http';
import {CarsServiceProvider} from "../../providers/cars-service/cars-service";
import {NavLifecycles} from "../../utils/ionic/nav/nav-lifecycles";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements NavLifecycles {
  public cars: Car[];

  constructor(
    public navCtrl: NavController,
    private _loadingCtrl: LoadingController,
    private _alert: AlertController,
    private carsService: CarsServiceProvider) {  }


  ionViewDidLoad(){
    let loading = this._loadingCtrl.create({
      content: 'Carregando Carros, aguarde...'
    });
    loading.present();

    this.carsService.list()
      .subscribe(
        (cars) => {
          this.cars = cars;
          loading.dismiss();
        },
        (err: HttpErrorResponse) => {
          loading.dismiss();
          this._alert.create({
            title: 'Falha na conexão',
            subTitle: 'Não foi possivel carregar a Lista de carros, tente novamente depois.',
            buttons: [
              { text: 'Ok' }
            ]
          }).present();


        }
      );
  }

}
