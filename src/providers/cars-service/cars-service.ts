import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Car} from "../../models/car";

/*
  Generated class for the CarsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CarsServiceProvider {

  constructor(private _http: HttpClient) {
  }

  list() {
    return this._http.get<Car[]>('http://localhost:8080/api/carro/listaTodos')
  }

}
